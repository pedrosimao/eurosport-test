/**
 * Europsport Test
 * by Pedro Simao
 */
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import HomePage from './components/pages/HomePage';
import PlayerPage from './components/pages/PlayerPage';

const App = createStackNavigator(
  {
    PlayerList: {
      screen: HomePage,
      navigationOptions: () => ({
        header: null,
      }),
    },
    PlayerDetails: PlayerPage,
  },
  {
    headerMode: 'screen',
    initialRouteName: 'PlayerList',
  },
);

export default createAppContainer(App);
