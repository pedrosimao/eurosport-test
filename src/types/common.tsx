interface PlayerCountry {
  picture: string;
  code: string;
}

interface PlayerData {
  rank: number;
  points: number;
  weight: number;
  height: number;
  age: number;
  last: Array<number>;
}

export interface Player {
  firstname: string;
  lastname: string;
  shortname: string;
  sex: string;
  country: PlayerCountry;
  picture: string;
  data: PlayerData;
}

interface Data {
  players?: [object];
}

export interface FetchResult {
  isLoading: boolean;
  data?: Data;
}
