import React from 'react';
import {StyleSheet, View} from 'react-native';
import _ from 'lodash';

import Label from '../../atoms/Label';

import {Player} from '../../../types/common';

interface Props {
  player: Player;
}

const PlayerInfo: React.FC<Props> = props => {
  const {player} = props;
  const playerData = _.get(player, 'data');
  const winCount = _.countBy(playerData.last, play => play === 1).true;
  const lossCount = _.countBy(playerData.last, play => play === 0).true;
  const weightKg = _.get(playerData, 'weight', 0) / 1000;
  return (
    <View style={styles.detailsContainer}>
      <Label style={styles.labelStyle}>Country: {player.country.code}</Label>
      <Label style={styles.labelStyle}>Sex: {player.sex}</Label>
      <Label style={styles.labelStyle}>Age: {playerData.age} years</Label>
      <Label style={styles.labelStyle}>Weight: {weightKg} Kg</Label>
      <Label style={styles.labelStyle}>Height: {playerData.height} cm</Label>
      <Label style={styles.labelStyle}>Points: {playerData.points}</Label>
      <Label style={styles.labelStyle}>Wins: {winCount}</Label>
      <Label style={styles.labelStyle}>Losses: {lossCount}</Label>
    </View>
  );
};

const styles = StyleSheet.create({
  detailsContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: 10,
  },
  labelStyle: {
    textAlign: 'center',
    fontSize: 18,
    fontWeight: '600',
    paddingVertical: 4,
    borderColor: 'rgba(0,0,0,0.2)',
    borderWidth: 1,
  },
});

export default PlayerInfo;
