import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Dimensions,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import useFetch from 'react-fetch-hook';
import _ from 'lodash';

import Header from '../../atoms/Header';
import Loader from '../../atoms/Loader';
import PlayerListItem from '../../organisms/PlayerListItem';

import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';
import {Player, FetchResult} from '../../../types/common';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const HomePage: React.FC<Props> = props => {
  const {isLoading, data = {}}: FetchResult = useFetch(
    'https://eurosportdigital.github.io/eurosport-react-native-developer-recruitment/docs/headtohead.json',
  );
  const playersByRank = _.orderBy(data.players, ['data.rank'], 'asc');
  return (
    <View style={styles.body}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Header />
        {isLoading ? (
          <Loader />
        ) : (
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                {_.map(playersByRank, (player: Player) => (
                  <PlayerListItem
                    key={player.shortname}
                    player={player}
                    navigation={props.navigation}
                  />
                ))}
              </View>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
    height: Dimensions.get('window').height - 100,
  },
  body: {
    backgroundColor: Colors.white,
    height: '100%',
    width: '100%',
  },
  sectionContainer: {
    marginTop: 5,
    paddingHorizontal: 30,
  },
});

export default HomePage;
