import React from 'react';
import {SafeAreaView, StyleSheet, View, StatusBar} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';

import PlayerDetails from '../../organisms/PlayerDetails';

import {Player} from '../../../types/common';

interface Props {
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}

const PlayerPage: React.FC<Props> = props => {
  const player: Player = props.navigation.getParam('player');
  return (
    <View style={styles.body}>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <PlayerDetails player={player} />
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  body: {
    backgroundColor: Colors.white,
    height: '100%',
    width: '100%',
  },
});

export default PlayerPage;
