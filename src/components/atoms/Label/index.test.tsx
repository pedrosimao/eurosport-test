import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import Label from '.';

const wrap = (props = {}) => shallow(<Label {...props} />);

it('renders correctly', () => {
  const tree = renderer.create(<Label>Testing</Label>).toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders children when passed in', () => {
  const wrapper = wrap({children: 'test'});
  expect(wrapper.contains('test')).toBe(true);
});

it('have the correct color when using prop "type"', () => {
  const wrapper = wrap({type: 'playerName', children: 'test'});
  expect(
    wrapper.find({'data-testid': 'atom-label'}).prop('style'),
  ).toHaveProperty('color', 'red');
});
