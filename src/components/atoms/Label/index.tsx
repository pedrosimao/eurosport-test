import React from 'react';
import {StyleSheet, Text} from 'react-native';
import _ from 'lodash';

export interface Props {
  children?: string | object;
  type?: string;
  style?: any;
}

const Label: React.FC<Props> = props => {
  const atomStyle: any = {
    ..._.get(styles, `${props.type}`),
    ..._.get(props, 'style'),
  };
  return (
    <Text data-testid="atom-label" style={atomStyle}>
      {props.children}
    </Text>
  );
};

// styles
const styles: any = StyleSheet.create({
  playerName: {
    color: 'red',
    fontWeight: 'bold',
    fontSize: 20,
  },
});
export default Label;
