import React from 'react';
import {shallow} from 'enzyme';
import renderer from 'react-test-renderer';
import Photo from '.';

const wrap = (props = {}) =>
  shallow(<Photo size={50} url={'http://url.com'} {...props} />);

it('renders correctly', () => {
  const tree = renderer
    .create(<Photo size={50} url={'http://url.com'} />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});


it('has the correct url', () => {
  const wrapper = wrap();
  expect(
    wrapper.find({'data-testid': 'photo-image'}).prop('source'),
  ).toMatchObject({uri: 'http://url.com'});
});

it('has the correct size', () => {
  const wrapper = wrap({size: 120});
  expect(
    wrapper.find({'data-testid': 'photo-image'}).prop('style'),
  ).toHaveProperty('width', 120);
  expect(
    wrapper.find({'data-testid': 'photo-image'}).prop('style'),
  ).toHaveProperty('borderRadius', 60);
});
