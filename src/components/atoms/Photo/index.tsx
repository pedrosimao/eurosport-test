import React from 'react';
import {StyleSheet, Image} from 'react-native';

export interface Props {
  url: string;
  size: number;
  style?: any;
}

const Photo: React.FC<Props> = props => {
  const {size, url, style} = props;
  return (
    <Image
      data-testid="photo-image"
      source={{uri: url}}
      style={{...styles(size).photoStyle, ...style}}
    />
  );
};

const styles: any = (size: number) =>
  StyleSheet.create({
    photoStyle: {
      width: size || 200,
      height: size || 200,
      borderRadius: size / 2,
    },
  });

export default Photo;
