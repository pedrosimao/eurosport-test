import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export interface Props {
  position: number;
  detail?: boolean;
  size?: number;
  style?: any;
}

const RankingPosition: React.FC<Props> = props => {
  const {position, style, detail} = props;
  // styles
  const styles: any = StyleSheet.create({
    viewStyle: {
      height: detail ? 80 : 50,
      width: detail ? 70 : 45,
    },
    trophyStyle: {
      color: detail ? 'white' : '#141b4D',
    },
    textStyle: {
      position: 'absolute',
      top: detail ? 16 : 12,
      alignSelf: 'center',
      color: detail ? 'black' : 'white',
      fontWeight: '700',
      fontSize: detail ? 40 : 20,
    },
  });
  return (
    <View style={{...styles.viewStyle, ...style}}>
      <Icon
        name="certificate"
        size={detail ? 80 : 50}
        style={{...styles.trophyStyle}}
      />
      <Text style={styles.textStyle}>{position}</Text>
    </View>
  );
};

export default RankingPosition;
