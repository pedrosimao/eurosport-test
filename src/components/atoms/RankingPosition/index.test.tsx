import React from 'react';
import renderer from 'react-test-renderer';
import RankingPosition from '.';

it('renders correctly', () => {
  const tree = renderer.create(<RankingPosition position={1} />).toJSON();
  expect(tree).toMatchSnapshot();
});
