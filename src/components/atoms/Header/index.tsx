import React from 'react';
import {StyleSheet, View, Image} from 'react-native';

const Header: React.FC = () => {
  return (
    <View style={styles.header}>
      <Image
        resizeMode="contain"
        source={require('../../../assets/logoBlue.png')}
        style={styles.logoStyle}
      />
    </View>
  );
};

const styles: any = StyleSheet.create({
  logoStyle: {
    height: 40,
    marginTop: 30,
    alignSelf: 'center',
  },
  header: {
    width: '100%',
    height: 100,
    backgroundColor: '#141b4D',
  },
});

export default Header;
