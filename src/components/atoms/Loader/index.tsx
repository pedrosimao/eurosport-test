import React from 'react';
import {View, ActivityIndicator, Dimensions, StyleSheet} from 'react-native';

const Loader: React.FC = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator
        size="large"
        color="#0000ff"
        style={styles.activityStyle}
      />
    </View>
  );
};

const styles: any = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: 'rgba(255,255,255,0.3)',
  },
  activityStyle: {
    top: Dimensions.get('window').height / 4,
  },
});

export default Loader;
