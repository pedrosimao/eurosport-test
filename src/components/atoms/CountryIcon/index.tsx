import React from 'react';
import {StyleSheet, Image} from 'react-native';

export interface Props {
  url: string;
  size?: string;
  style?: any;
}

const CountryIcon: React.FC<Props> = props => {
  return (
    <Image
      data-testid="country-image"
      source={{uri: props.url}}
      style={{...styles(props.size).iconStyle, ...props.style}}
    />
  );
};

// styles
const styles: any = (size: string) => {
  const iconSize = () => {
    switch (size) {
      case 'sm':
        return 20;
      case 'md':
        return 40;
      case 'lg':
        return 60;
      default:
        return 40;
    }
  };
  return StyleSheet.create({
    iconStyle: {
      width: iconSize(),
      height: iconSize(),
      borderRadius: iconSize() / 2,
    },
  });
};

export default CountryIcon;
