import React from 'react';
import renderer from 'react-test-renderer';
import {shallow} from 'enzyme';
import CountryIcon from '.';

const wrap = (props = {}) =>
  shallow(<CountryIcon url={'http://url.com'} {...props} />);

it('renders correctly', () => {
  const tree = renderer
    .create(<CountryIcon url={'http://url.com'} size="sm" />)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

it('has the correct url', () => {
  const wrapper = wrap({size: 'sm'});
  expect(
    wrapper.find({'data-testid': 'country-image'}).prop('source'),
  ).toMatchObject({uri: 'http://url.com'});
});

it('has the correct size for "sm"', () => {
  const wrapper = wrap({size: 'sm'});
  expect(
    wrapper.find({'data-testid': 'country-image'}).prop('style'),
  ).toHaveProperty('height', 20);
});

it('has the correct size for "lg"', () => {
  const wrapper = wrap({size: 'lg'});
  expect(
    wrapper.find({'data-testid': 'country-image'}).prop('style'),
  ).toHaveProperty('height', 60);
});
