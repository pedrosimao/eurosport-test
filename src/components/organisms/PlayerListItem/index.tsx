import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import _ from 'lodash';

import Label from '../../atoms/Label';
import Photo from '../../atoms/Photo';
import CountryIcon from '../../atoms/CountryIcon';
import RankingPosition from '../../atoms/RankingPosition';

import {Player} from '../../../types/common';
import {
  NavigationParams,
  NavigationScreenProp,
  NavigationState,
} from 'react-navigation';

interface Props {
  player: Player;
  navigation: NavigationScreenProp<NavigationState, NavigationParams>;
}
const PlayerListItem: React.FC<Props> = props => {
  const {player, navigation} = props;
  return (
    <TouchableOpacity
      key={player.shortname}
      onPress={() => navigation.navigate('PlayerDetails', {player})}>
      <View style={styles.sectionContainer}>
        <Photo size={120} url={_.get(player, 'picture')} />
        <View style={styles.contentStyle}>
          <RankingPosition position={_.get(player, 'data.rank')} />
          <Label type="playerName" style={styles.sectionTitle}>
            {player.firstname} {player.lastname}
          </Label>
        </View>
        <CountryIcon
          url={_.get(player, 'country.picture')}
          size="md"
          style={styles.iconStyle}
        />
      </View>
      <View style={styles.separator} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: 10,
  },
  iconStyle: {
    position: 'absolute',
    bottom: -5,
  },
  contentStyle: {
    marginLeft: 10,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: 'black',
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: 'black',
    opacity: 0.2,
  },
});

export default PlayerListItem;
