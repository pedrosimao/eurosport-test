import React from 'react';
import {StyleSheet, View} from 'react-native';
import _ from 'lodash';

import Label from '../../atoms/Label';
import Photo from '../../atoms/Photo';
import PlayerInfo from '../../molecules/PlayerInfo';
import RankingPosition from '../../atoms/RankingPosition';

import {Player} from '../../../types/common';

interface Props {
  player: Player;
}
const PlayerDetails: React.FC<Props> = props => {
  const {player} = props;
  return (
    <React.Fragment>
      <View style={styles.sectionContainer}>
        <RankingPosition
          position={_.get(player, 'data.rank')}
          style={styles.rankingStyle}
          detail
        />
        <Photo
          size={120}
          url={_.get(player, 'picture')}
          style={styles.photoStyle}
        />
        <View style={styles.contentStyle}>
          <Label type="playerName" style={styles.sectionTitle}>
            {player.firstname} {player.lastname}
          </Label>
        </View>
      </View>
      <View style={styles.separator} />
      <PlayerInfo player={player} />
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    display: 'flex',
    flexDirection: 'column',
    paddingVertical: 5,
    alignItems: 'center',
    backgroundColor: '#141b4D',
  },
  contentStyle: {
    marginLeft: 10,
  },
  sectionTitle: {
    fontSize: 35,
    fontWeight: '700',
    color: 'white',
    marginTop: 10,
  },
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: 'black',
    opacity: 0.2,
  },
  photoStyle: {
    width: 220,
    height: 220,
    borderRadius: 110,
    borderColor: 'white',
    borderWidth: 5,
  },
  rankingStyle: {
    position: 'absolute',
    left: 70,
    zIndex: 10,
  },
});

export default PlayerDetails;
